(function( $, window, document, undefined ){
	var Module1 = function(){

		// Приватные свойства
		var privateProperties = {};
		privateProperties.privateProperty = 'Private property';

		// Публичные свойства
		var publicProperty = 'Public property.';

		// Приватные методы
		var privateMethod = function(){
			console.log( privateProperties.privateProperty );
		};

		return {

			// Публичные методы
			publicMethod : function(){
				privateMethod();
			},

			// публичные переменные
			publicProperty : publicProperty

		};
	}();

	// Export
	window.Module1 = Module1;
	
})( jQuery, this, this.document );