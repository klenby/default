'use strict';

(function( $, window, document, undefined ){

	$(function(){

		// Импорт модуля
		var Module1 = window.Module1;

		// Вызов публичного метода модуля
		Module1.publicMethod();

		console.log( Module1.publicProperty );
		
	});	

})( jQuery, this, this.document );
