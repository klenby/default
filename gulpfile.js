/**
 * ===================== SETTINGS ========================
 */
/**
 * Библиотеки CSS
 */
var libCSS = [
	// 'src/assets/fonts/stylesheet.css',
	// 'node_modules/bootstrap/dist/css/bootstrap.css'
];

/**
 * Библиотеки JS
 */
var libJS = [
	'node_modules/jquery/dist/jquery.min.js',
];

/**
 * Версии поддерживаемых браузеров
 */
var platform = ['last 4 versions', 'ie >= 10', 'iOS >= 8'];

/**
 * Включить поддержку GRID в ИЕ 10-11
 * https://css-tricks.com/css-grid-in-ie-css-grid-and-the-new-autoprefixer/
 * 
 * Не поддерживаются:
 * grid-auto-columns
 * grid-auto-rows
 * grid-auto-flow
 * grid
 * 
 */
var useGrid = true;

/**
 * Объединять код приложения и вендоров?
 */
var isOneJS = true;

/* ======================================================== */
/* global require */
var gulp = require('gulp'); // Собственно gulp

var less = require('gulp-less'); // Less-компилятор
var postcss = require('gulp-postcss'); // Пост-обработка CSS
var autoprefixer = require('autoprefixer'); // Добавление вендорных префиксов
var minifyCSS = require('gulp-csso'); // CSS минификатор
var sourcemaps = require('gulp-sourcemaps'); // карты исходного кода

var concat = require('gulp-concat'); // объединение набора файлов в один
var uglify = require('gulp-uglify'); // JS минификатор

var imagemin = require('gulp-imagemin');  // оптимизация картинок
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var pngquant = require('imagemin-pngquant');
var cache = require('gulp-cache');

var include = require( 'gulp-file-include' ); // включение файлов
var typograf = require('gulp-typograf'); // типографика

var clean = require('gulp-clean'); // удаление файлов и каталогов
// var sftp = require('gulp-sftp'); // SFTP

var browserSync = require('browser-sync').create(); // обновление налету

// ================== CLEAN ============================
// Очистка dev-папки
gulp.task( 'dev:clean', function(){
	return gulp.src( 'dev/*', { read: false } )
		.pipe( clean() );
});

// Очистка prod-папки
gulp.task( 'prod:clean', function(){
	return gulp.src( 'prod/*', { read: false } )
		.pipe( clean() );
});

// Полная очистка
gulp.task( 'CLEAN', [ 'dev:clean', 'prod:clean' ] );

// ================== LESS ============================

// Собираем все CSS библиотеки
gulp.task( 'dev:css-libs', function(){
	return gulp.src( libCSS )
		.pipe( concat( 'vendor.css' ) )
		.pipe( minifyCSS() )
		.pipe( gulp.dest( 'dev/assets/css' ) );
});

// Очищаем папку с CSS
gulp.task( 'dev:css-clean', function(){
	return gulp.src( 'dev/assets/css', { read: false } )
		.pipe( clean() );
});

// Собираем CSS
gulp.task( 'dev:css', [ 'dev:css-libs' ], function(){
	// Настройки для вендорных префиксов
	var plugins = [
		autoprefixer({
			grid: useGrid,
			browsers: platform
		}),
	];
	return gulp.src( 'src/assets/less/app.less' )
		.pipe( sourcemaps.init() )
		.pipe( less() )
		.pipe( postcss( plugins ) )
		.pipe( sourcemaps.write( '.' ) )
		.pipe( gulp.dest( 'dev/assets/css' ) )
		.pipe( browserSync.stream() );
});

// Собираем все CSS библиотеки
gulp.task( 'prod:css-libs', function(){
	return gulp.src( libCSS )
		.pipe( concat( 'vendor.css' ) )
		.pipe( minifyCSS() )
		.pipe( gulp.dest( 'prod/assets/css' ) );
});

// Очищаем папку с CSS
gulp.task( 'prod:css-clean', function(){
	return gulp.src( 'prod/css', { read: false } )
		.pipe( clean() );
});

// Собираем CSS
gulp.task( 'prod:css', [ 'prod:css-libs' ], function(){
	// Настройки для вендорных префиксов
	var plugins = [
		autoprefixer({
			grid: useGrid,
			browsers: platform
		}),
	];
	return gulp.src( 'src/assets/less/app.less' )
		.pipe( less() )
		.pipe( postcss( plugins ) )
		.pipe( minifyCSS() )
		.pipe( gulp.dest( 'prod/assets/css' ) );
});

// ================== FONTS ============================
gulp.task( 'dev:fonts', function(){
	return gulp.src( 'src/assets/fonts/*' )
		.pipe( gulp.dest( 'dev/assets/fonts' ) );
});

gulp.task( 'prod:fonts', function(){
	return gulp.src( 'src/assets/fonts/*' )
		.pipe( gulp.dest( 'prod/assets/fonts' ) );
});

// ================== JS ============================

// стираем все js файлы из dev
gulp.task( 'dev:js-clean', function(){
	return gulp.src( 'dev/assets/js', { read: false } )
		.pipe( clean() );
});

// Собираем все JS библиотеки
gulp.task( 'dev:js-libs', function(){
	return gulp.src( libJS )
		.pipe( concat( 'vendor.js' ) )
		.pipe( gulp.dest( 'dev/assets/js' ) );
});

// Компилируем и собираем все js файлы проекта
gulp.task( 'dev:js-app', function(){
	return gulp.src( [ 'src/components/**/*.js', 'src/assets/js/app.js' ] )
		.pipe( sourcemaps.init() )
		.pipe( concat( 'app.js' ) )
		.pipe( sourcemaps.write( '.' ) )
		.pipe( gulp.dest( 'dev/assets/js' ) );
});

// Обрабатываем js файлы библиотек и проекта
// Если isOneJS = true, то объединяем все файлы в один.
gulp.task( 'dev:js', ['dev:js-libs', 'dev:js-app'], function(){
	if( isOneJS ){
		return gulp.src( ['dev/assets/js/vendor.js', 'dev/assets/js/app.js'] )
			.pipe( concat( 'app.js' ) )
			.pipe( gulp.dest( 'dev/assets/js' ) );
	}
});

// стираем все js файлы из prod
gulp.task( 'prod:js-clean', function(){
	return gulp.src( 'prod/assets/js', { read: false } )
		.pipe( clean() );
});

// Собираем все JS библиотеки
gulp.task( 'prod:js-libs', function(){
	return gulp.src( libJS )
		.pipe( concat( 'vendor.js' ) )
		.pipe( gulp.dest( 'prod/assets/js' ) );
});

// Компилируем и собираем все js файлы проекта
gulp.task( 'prod:js-app', function(){
	return gulp.src( [ 'src/components/**/*.js', 'src/assets/js/app.js' ] )
		.pipe( concat( 'app.js' ) )
		.pipe( uglify() )
		.pipe( gulp.dest( 'prod/assets/js' ) );
});

// Обрабатываем js файлы библиотек и проекта
// Если isOneJS = true, то объединяем все файлы в один.
gulp.task( 'prod:js', ['prod:js-libs', 'prod:js-app'], function(){
	if( isOneJS ){
		return gulp.src( ['prod/assets/js/vendor.js', 'prod/assets/js/app.js'] )
			.pipe( concat( 'app.js' ) )
			.pipe( gulp.dest( 'prod/assets/js' ) );
	}
} );


// ================== IMG ============================
// Оптимизация картинок для dev
gulp.task('dev:img', function() {
	return gulp.src( [
		'src/assets/img/**/*',
		'src/components/**/*.png',
		'src/components/**/*.jpg',
		'src/components/**/*.jpeg',
		'src/components/**/*.gif',
		'src/components/**/*.svg' ] )
		.pipe(
			cache(
				imagemin( [
					imagemin.gifsicle( {interlaced: true} ),
					imagemin.jpegtran( {progressive: true} ),
					imageminJpegRecompress({
						quality: 'high'
					}),
					imagemin.svgo(),
					imagemin.optipng( {optimizationLevel: 3} ),
					pngquant( {quality: '92', speed: 5} )
				],{
					verbose: true
				})
			)
		)
		.pipe(gulp.dest( 'dev/assets/img' ));
});

// Оптимизация картинок для prod
gulp.task('prod:img', function() {
	return gulp.src( [
		'src/assets/img/**/*',
		'src/components/**/*.png',
		'src/components/**/*.jpg',
		'src/components/**/*.jpeg',
		'src/components/**/*.gif',
		'src/components/**/*.svg' ] )
		.pipe(
			cache(
				imagemin( [
					imagemin.gifsicle( {interlaced: true} ),
					imagemin.jpegtran( {progressive: true} ),
					imageminJpegRecompress({
						quality: 'high'
					}),
					imagemin.svgo(),
					imagemin.optipng( {optimizationLevel: 3} ),
					pngquant( {quality: '92', speed: 5} )
				],{
					verbose: true
				})
			)
		)
		.pipe(gulp.dest( 'prod/assets/img' ));
});


// ================== HTML ============================
// HTML для dev
gulp.task( 'dev:html', function(){
	return gulp.src( ['src/**/*.html', '!src/assets/*.html', '!src/components/**/*.html'] )
		.pipe( include({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(typograf({ locale: ['ru', 'en-US'] }))
		.pipe( gulp.dest( 'dev' ) );
});

// HTML для prod
gulp.task( 'prod:html', function(){
	return gulp.src( ['src/**/*.html', '!src/assets/*.html', '!src/components/**/*.html'] )
		.pipe( include({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(typograf({ locale: ['ru', 'en-US'] }))
		.pipe( gulp.dest( 'prod' ) );
});

// ================== DEV ============================
// Слежение за dev
gulp.task( 'DEV', [ 'dev:img', 'dev:fonts', 'dev:css', 'dev:js', 'dev:html' ], function() {

	browserSync.init({
		server: {
			baseDir: './dev/'
		}
	});

	gulp.watch( 'src/assets/less/*.less' , ['dev:css']);
	gulp.watch( 'src/components/**/*.less' , ['dev:css']);

	gulp.watch( 'src/assets/js/*.js', [ 'dev:js' ] );
	gulp.watch( 'src/components/**/*.js', [ 'dev:js' ] );
	gulp.watch( 'dev/assets/js/**/*' ).on('change', browserSync.reload);

	gulp.watch( 'src/**/*.html', [ 'dev:html' ] );
	gulp.watch( 'src/components/**/*.html', [ 'dev:html' ] );
	gulp.watch( 'dev/*.html' ).on('change', browserSync.reload);

	gulp.watch( 'src/assets/img/**/*', [ 'dev:img' ] );
	gulp.watch( 'src/components/**/*.gif', [ 'dev:img' ] );
	gulp.watch( 'src/components/**/*.jpg', [ 'dev:img' ] );
	gulp.watch( 'src/components/**/*.jpeg', [ 'dev:img' ] );
	gulp.watch( 'src/components/**/*.png', [ 'dev:img' ] );
	gulp.watch( 'src/components/**/*.svg', [ 'dev:img' ] );
	gulp.watch( 'dev/assets/img/**/*' ).on( 'change', browserSync.reload );
});

// ================== PROD ============================
// Публикация на prod
gulp.task( 'PROD', [ 'prod:img', 'prod:fonts', 'prod:css', 'prod:js', 'prod:html' ], function(){

	//return gulp.src('prod/**/*')
	//	.pipe(sftp({
	//		host: 'cp412.agava.net',
	//		user: '',
	//		pass: '',
	//		remotePath: '/home/alfasa22/public_html/'
	//	}));
});

// ================== DEFAULT ============================
gulp.task( 'default', [ 'DEV' ] );